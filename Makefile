

BASE=template.sql

plrmore.sql: plrmore_float4.sql plrmore_int8.sql plrmore_int4.sql plrmore_int2.sql
	cat template.sql > $@
	cat plrmore_int8.sql >> $@
	cat plrmore_int4.sql >> $@
	cat plrmore_int2.sql >> $@


plrmore_float4.sql: $(BASE)
	sed -e 's/float8/float4/' $(BASE) > $@

plrmore_int8.sql: $(BASE)
	sed -e 's/float8/int8/' $(BASE) > $@

plrmore_int4.sql: $(BASE)
	sed -e 's/float8/int4/' $(BASE) > $@

plrmore_int2.sql: $(BASE)
	sed -e 's/float8/int2/' $(BASE) > $@

