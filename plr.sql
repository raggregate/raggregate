-- Copyright (C) 2009-2010 Rodolphe Quiédeville <rodolphe@quiedeville.org>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--
-- Create procedural language PL/R and median aggregate function
--
-- You need special privileges on the database to create a new langague
-- as described in http://www.postgresql.org/docs/8.3/static/sql-createlanguage.html
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = error;
SET escape_string_warning = off;
--
-- Library handler
--

DROP FUNCTION IF EXISTS plr_call_handler() CASCADE ; 

CREATE FUNCTION plr_call_handler()
       RETURNS LANGUAGE_HANDLER
       AS '$libdir/plr' 
       LANGUAGE C;

CREATE LANGUAGE plr HANDLER plr_call_handler;

--
-- Functions
--
DROP FUNCTION IF EXISTS plr_array_accum (_float8, float8) CASCADE ; 
DROP FUNCTION IF EXISTS plr_array_accum (_float4, float4) CASCADE ; 
DROP FUNCTION IF EXISTS plr_array_accum (_int8, int8) CASCADE ; 
DROP FUNCTION IF EXISTS plr_array_accum (_int4, int4) CASCADE ; 
DROP FUNCTION IF EXISTS plr_array_accum (_int2, int2) CASCADE ; 
DROP FUNCTION IF EXISTS plr_array_accum (_numeric, numeric) CASCADE ; 

CREATE FUNCTION plr_array_accum (_float8, float8)
       RETURNS float8[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

CREATE FUNCTION plr_array_accum (_float4, float4)
       RETURNS float4[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

CREATE FUNCTION plr_array_accum (_int8, int8)
       RETURNS int8[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

CREATE FUNCTION plr_array_accum (_int4, int4)
       RETURNS int4[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

CREATE FUNCTION plr_array_accum (_int2, int2)
       RETURNS int2[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

CREATE FUNCTION plr_array_accum (_numeric, numeric)
       RETURNS numeric[]
       AS '$libdir/plr','plr_array_accum'
       LANGUAGE 'C';

--
-- median
--
DROP FUNCTION IF EXISTS r_median(_float8) CASCADE ; 
DROP FUNCTION IF EXISTS r_median(_float4) CASCADE ; 
DROP FUNCTION IF EXISTS r_median(_int8) CASCADE ; 
DROP FUNCTION IF EXISTS r_median(_int4) CASCADE ; 
DROP FUNCTION IF EXISTS r_median(_int2) CASCADE ; 
DROP FUNCTION IF EXISTS r_median(_numeric) CASCADE ; 


CREATE FUNCTION r_median(_float8) 
       RETURNS float8 
       AS 'median(arg1)' 
       LANGUAGE 'plr';

CREATE FUNCTION r_median(_float4) 
       RETURNS float4
       AS 'median(arg1)' 
       LANGUAGE 'plr';

CREATE FUNCTION r_median(_int8) 
       RETURNS float8 
       AS 'median(arg1)' 
       LANGUAGE 'plr';

CREATE FUNCTION r_median(_int4) 
       RETURNS float4
       AS 'median(arg1)' 
       LANGUAGE 'plr';

CREATE FUNCTION r_median(_int2) 
       RETURNS float4
       AS 'median(arg1)' 
       LANGUAGE 'plr';

CREATE FUNCTION r_median(_numeric) 
       RETURNS numeric
       AS 'median(arg1)' 
       LANGUAGE 'plr';
--
-- Aggregate
--
CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = float8,
       stype = _float8,
       finalfunc = r_median
);

CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = float4,
       stype = _float4,
       finalfunc = r_median
);

CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = numeric,
       stype = _numeric,
       finalfunc = r_median
);

CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = int8,
       stype = _int8,
       finalfunc = r_median
);

CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = int4,
       stype = _int4,
       finalfunc = r_median
);

CREATE AGGREGATE median (
       sfunc = plr_array_accum,
       basetype = int2,
       stype = _int2,
       finalfunc = r_median
);


DROP AGGREGATE IF EXISTS quartile4 (float4) CASCADE;

DROP FUNCTION IF EXISTS r_quartile4(_float4) CASCADE;


CREATE FUNCTION r_quartile4(_float4) 
       RETURNS float4
       AS 'quantile(arg1,1)' 
       LANGUAGE 'plr';

CREATE AGGREGATE quartile4 (float4) (
       sfunc = plr_array_accum,
       stype = _float4,
       finalfunc = r_quartile4
);


DROP AGGREGATE IF EXISTS quartile0 (float8) CASCADE;
DROP AGGREGATE IF EXISTS quartile1 (float8) CASCADE;
DROP AGGREGATE IF EXISTS quartile2 (float8) CASCADE;
DROP AGGREGATE IF EXISTS quartile3 (float8) CASCADE;
DROP AGGREGATE IF EXISTS quartile4 (float8) CASCADE;

DROP FUNCTION IF EXISTS r_quartile0(_float8) CASCADE;
DROP FUNCTION IF EXISTS r_quartile1(_float8) CASCADE;
DROP FUNCTION IF EXISTS r_quartile2(_float8) CASCADE;
DROP FUNCTION IF EXISTS r_quartile3(_float8) CASCADE;
DROP FUNCTION IF EXISTS r_quartile4(_float8) CASCADE;


CREATE FUNCTION r_quartile0(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0)' 
       LANGUAGE 'plr';

CREATE AGGREGATE quartile0 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_quartile0
);

CREATE FUNCTION r_quartile1(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.25)' LANGUAGE 'plr';

CREATE AGGREGATE quartile1 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_quartile1
);

CREATE FUNCTION r_quartile2(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.50)' LANGUAGE 'plr';

CREATE AGGREGATE quartile2 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_quartile2
);



CREATE FUNCTION r_quartile3(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.75)' LANGUAGE 'plr';

CREATE AGGREGATE quartile3 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_quartile3
);


CREATE FUNCTION r_quartile4(_float8) 
       RETURNS float8
       AS 'quantile(arg1,1)' LANGUAGE 'plr';

CREATE AGGREGATE quartile4 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_quartile4
);
--
-- iqr
--
DROP AGGREGATE IF EXISTS iqr (float8) CASCADE;
DROP FUNCTION IF EXISTS r_iqr(_float8) CASCADE;

CREATE OR REPLACE FUNCTION r_iqr(_float8) RETURNS float AS '
  IQR(arg1)
' LANGUAGE 'plr';

CREATE AGGREGATE iqr (
  sfunc = plr_array_accum,
  basetype = float8,
  stype = _float8,
  finalfunc = r_iqr
);   

--
-- quantile
--

DROP AGGREGATE IF EXISTS quantile (float8) CASCADE;

DROP FUNCTION IF EXISTS r_quantile(_float8) CASCADE;


CREATE OR REPLACE FUNCTION r_quantile(float8[])
       RETURNS float8[] 
       AS 'quantile(arg1, probs = seq(0, 1, 0.25), names = FALSE)' LANGUAGE 'plr';

CREATE AGGREGATE quantile (float8) (
    sfunc = plr_array_accum,
    stype = float8[],
    finalfunc = r_quantile
);
