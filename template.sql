-- Copyright (C) 2009-2010 Rodolphe Quiédeville <rodolphe@quiedeville.org>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--
-- Create procedural language PL/R and median aggregate function
--
-- You need special privileges on the database to create a new langague
-- as described in http://www.postgresql.org/docs/8.3/static/sql-createlanguage.html
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = error;
SET escape_string_warning = off;

--- ----------------------------------------------------------------------- ---
---                                                                         ---
---                   float8                                                ---
---                                                                         ---
--- ----------------------------------------------------------------------- ---

DROP AGGREGATE IF EXISTS centile99 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile99(_float8) CASCADE;

CREATE FUNCTION r_centile99(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.99)' LANGUAGE 'plr';

CREATE AGGREGATE centile99 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile99
);

DROP AGGREGATE IF EXISTS centile97_5 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile97_5(_float8) CASCADE;

CREATE FUNCTION r_centile97_5(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.975)' LANGUAGE 'plr';

CREATE AGGREGATE centile97_5 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile97_5
);

DROP AGGREGATE IF EXISTS centile95 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile95(_float8) CASCADE;

CREATE FUNCTION r_centile95(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.95)' LANGUAGE 'plr';

CREATE AGGREGATE centile95 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile95
);

DROP AGGREGATE IF EXISTS centile92_5 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile92_5(_float8) CASCADE;

CREATE FUNCTION r_centile92_5(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.925)' LANGUAGE 'plr';

CREATE AGGREGATE centile92_5 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile92_5
);

DROP AGGREGATE IF EXISTS centile90 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile90(_float8) CASCADE;

CREATE FUNCTION r_centile90(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.90)' LANGUAGE 'plr';

CREATE AGGREGATE centile90 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile90
);
--
--
--

DROP AGGREGATE IF EXISTS centile10 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile10(_float8) CASCADE;

CREATE FUNCTION r_centile10(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.10)' LANGUAGE 'plr';

CREATE AGGREGATE centile10 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile10
);
--
--
DROP AGGREGATE IF EXISTS centile7_5 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile7_5(_float8) CASCADE;

CREATE FUNCTION r_centile7_5(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.025)' LANGUAGE 'plr';

CREATE AGGREGATE centile7_5 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile7_5
);
--
--
--
DROP AGGREGATE IF EXISTS centile5 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile5(_float8) CASCADE;

CREATE FUNCTION r_centile5(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.050)' LANGUAGE 'plr';

CREATE AGGREGATE centile5 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile5
);
--
--
--
DROP AGGREGATE IF EXISTS centile2_5 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile2_5(_float8) CASCADE;

CREATE FUNCTION r_centile2_5(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.025)' LANGUAGE 'plr';

CREATE AGGREGATE centile2_5 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile2_5
);
--
--
--
DROP AGGREGATE IF EXISTS centile1 (float8) CASCADE;
DROP FUNCTION IF EXISTS r_centile1(_float8) CASCADE;

CREATE FUNCTION r_centile1(_float8) 
       RETURNS float8
       AS 'quantile(arg1,0.001)' LANGUAGE 'plr';

CREATE AGGREGATE centile1 (float8) (
       sfunc = plr_array_accum,
       stype = _float8,
       finalfunc = r_centile1
);

